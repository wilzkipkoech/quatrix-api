package com.wilson.quartix.quatrixapi.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wilson.quartix.quatrixapi.model.UserInfo;

import java.util.List;


@Repository
public interface UserInfoRepository extends JpaRepository<UserInfo,Integer> {

    Boolean existsByphone(String phone);
    UserInfo findByphone(String phone);


}

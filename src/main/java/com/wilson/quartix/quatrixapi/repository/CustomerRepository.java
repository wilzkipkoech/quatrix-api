package com.wilson.quartix.quatrixapi.repository;

import java.util.Collection;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.wilson.quartix.quatrixapi.controller.CustomerController;
import com.wilson.quartix.quatrixapi.model.Customer;

@Repository
public interface CustomerRepository extends PagingAndSortingRepository<Customer, Integer> {
	
	Customer findById(int id);

	

}



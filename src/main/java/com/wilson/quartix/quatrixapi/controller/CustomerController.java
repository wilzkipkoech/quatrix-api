package com.wilson.quartix.quatrixapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.wilson.quartix.quatrixapi.model.Customer;
import com.wilson.quartix.quatrixapi.repository.CustomerRepository;
import com.wilson.quartix.quatrixapi.service.CustomerService;
import com.wilson.quartix.quatrixapi.service.CustomerServiceImpl;


@CrossOrigin(origins = { "http://localhost:3000" })
@Configuration
@RestController
@Import({CustomerServiceImpl.class})
public class CustomerController {
	@Autowired
	 CustomerService customerService;
	 
	 @PostMapping("/post/task")
	 public HttpStatus createCustomer(@Validated @RequestBody Customer customer) {
		 customerService.saveCustomer(customer);
		return HttpStatus.CREATED;
	}
	
	
	@GetMapping("/task/assigned")
	public List<Customer> findAll(){
		return customerService.getAllCustomers();
	}
	
	@GetMapping("/task")
	public String Hello() {
	return "I love you Nancy";	
	}
	
//	public GenericRes (Generic Req) {
//		
//		req.getToken
//		req.pass
//		req.getData<List<Customer>
//		
//		
//		
//		
//		
//	}
	
}

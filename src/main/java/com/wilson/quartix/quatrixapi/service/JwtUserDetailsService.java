package com.wilson.quartix.quatrixapi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.wilson.quartix.quatrixapi.model.UserInfo;
import com.wilson.quartix.quatrixapi.repository.UserInfoRepository;

import java.util.ArrayList;

@Component
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserInfoRepository userInfoRepository;


    @Override
    public UserDetails loadUserByUsername(String phone) throws UsernameNotFoundException {

        UserInfo user = userInfoRepository.findByphone(phone);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with Phone Number: " + phone);
        }
        return new org.springframework.security.core.userdetails.User(user.getPhone(), user.getPassword(),
                new ArrayList<>());
    }

}
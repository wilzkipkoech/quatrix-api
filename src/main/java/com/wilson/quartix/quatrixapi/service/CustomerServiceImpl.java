package com.wilson.quartix.quatrixapi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


import com.wilson.quartix.quatrixapi.model.Customer;
import com.wilson.quartix.quatrixapi.repository.CustomerRepository;



public class CustomerServiceImpl implements CustomerService {
	
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Override
	public List<Customer> getAllCustomers() {
		return (List<Customer>) customerRepository.findAll();
	}

	@Override
	public void saveCustomer(Customer customer) {
		customerRepository.save(customer);
		
	}
	
//	@Override
//	public void saveCustomer(Customer customer) {
//		customerRepository.save(customer);
//	}

}
